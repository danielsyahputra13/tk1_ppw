from django.urls import path
from . import views

app_name= 'info'

urlpatterns = [
    path('', views.index, name='info'),
    path('patient/', views.patient, name='patient'),
    path("details/<int:pk>", views.patient_details, name="patient_details"),
    path("delete/<int:pk>", views.delete_patient, name="delete_patient"),
    path('rumah_sakit/', views.daftar_rumah_sakit, name="daftar_rumah_sakit"),
]
