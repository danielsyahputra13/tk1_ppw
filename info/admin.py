from django.contrib import admin
from .models import KasusProvinsi, RumahSakitRujukan

# Register your models here.
admin.site.register(KasusProvinsi)
admin.site.register(RumahSakitRujukan)