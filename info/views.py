import requests

from django.contrib import messages
from django.shortcuts import render, redirect

from form.models import Patient

from .models import KasusProvinsi, RumahSakitRujukan
from .helper import rumah_sakit_helper, kasus_provinsi_helper


def index(request):
    response = requests.get('https://api.kawalcorona.com/indonesia/provinsi')
    data = response.json()
    kasus_provinsi_helper(data)
    data_semua_kasus = KasusProvinsi.objects.all()
    return render(request, 'info/index.html', {
        "semua_data" : data_semua_kasus
    })

def patient(request):
    patients = Patient.objects.all()
    is_empty = False
    if not patients:
        is_empty = True
    context = {
        'patients': patients,
        'is_empty': is_empty,
    }
    return render(request, 'info/data.html', context)

def patient_details(request, pk):
    patient = Patient.objects.get(id=pk)
    context = {'patient': patient}
    return render(request, 'info/details.html', context)

def delete_patient(request, pk):
    if request.method == "POST":
        patient = Patient.objects.get(id=pk)
        patient.delete()
        messages.success(request, (f"{patient} deleted."))
    return redirect('info:patient')

def daftar_rumah_sakit(request):
    response = requests.get('https://dekontaminasi.com/api/id/covid19/hospitals')
    data = response.json()
    rumah_sakit_helper(data)
    daftar_semua_rumah_sakit = RumahSakitRujukan.objects.all()
    return render(request, "info/rumah_sakit.html", {
        "rumah_sakit_rujukan": daftar_semua_rumah_sakit
    })
