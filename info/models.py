from django.db import models

# Create your models here.
class KasusProvinsi(models.Model):
    provinsi = models.CharField(max_length=50)
    kasus_positif = models.IntegerField()
    kasus_sembuh = models.IntegerField()
    kasus_meninggal = models.IntegerField()

    def __str__(self):
        return self.provinsi

class RumahSakitRujukan(models.Model):
    rumah_sakit = models.CharField(max_length=100)
    alamat = models.CharField(max_length=500)
    region = models.CharField(max_length=500)
    nomor_telepon = models.CharField(max_length=100, blank=True, null=True)
    provinsi = models.CharField(max_length=200)

    def __str__(self):
        return self.rumah_sakit
