from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse

# Create your tests here.
class UserModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
    
    def test_user_count(self):
        self.assertEqual(User.objects.count(), 1)
    
    def test_user_fields(self):
        user = User.objects.create_user(
            username='danielsyahputra',
            email= 'danielsyahputra@email.com',
            password='11111111'
        )
        self.assertEqual(user.username, 'danielsyahputra')
        self.assertEqual(user.email, 'danielsyahputra@email.com')

class UrlsPathTest(TestCase):
    def setUp(self):
        self.login_page = reverse('login')
        self.logout_page = reverse('logout')
        self.signup_page = reverse('login_register:signup')

    def test_login_matching_with_the_right_path(self):
        self.assertEqual(self.login_page, '/accounts/login/')

    def test_logout_matching_with_the_right_path(self):
        self.assertEqual(self.logout_page, '/accounts/logout/')

    def test_signup_matching_with_the_right_path(self):
        self.assertEqual(self.signup_page, '/accounts/signup/')

class LoginPageTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
        self.user.save()
    
    def test_login_page_status_code(self):
        c = Client()
        response = c.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_view_by_name(self):
        c = Client()
        response = c.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
    
    def test_login_page_uses_correct_template(self):
        c = Client()
        response = c.get(reverse('login'))
        self.assertTemplateUsed(response, 'registration/login.html')
    
    def test_login_post_valid(self):
        c = Client()
        response = c.post(
            '/accounts/login/', {
                "username" : 'testuser',
                "password" : '12345678',
            }, follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home/index.html')

class SignUpPageTest(TestCase):
    def setUp(self):
        self.signup_page = reverse('login_register:signup')
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )

    def test_GET_signup_page(self):
        c = Client()
        response = c.get(self.signup_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login_register/signup.html')
        self.assertContains(response, 'Create a new account')
    
    def test_POST_signup_page_valid(self):
        c = Client()
        response = c.post(
            self.signup_page, {
            'username': 'daniel123',
            'password1': 'awertyuh67',
            'password2': 'awertyuh67',
            }, follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), 2)
        self.assertContains(response, 'Sign in to your account')
