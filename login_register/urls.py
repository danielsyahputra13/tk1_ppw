from django.urls import path

from .views import sign_up_page

app_name= 'login_register'

urlpatterns = [
    path('signup/', sign_up_page, name='signup'),
]