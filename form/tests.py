from django.test import Client, TestCase
from django.urls import resolve, reverse

from .models import Patient


class FormTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.form_url = reverse('form:form')
        self.success_url = reverse('form:success')
        self.patient = Patient.objects.create(
            name='Andre',
            age=30,
            sex='Male',
            job='Petani',
            city='Depok',
            province='Jawa Barat',
            medical_history='Asma',
            education='SMA',
            symptoms='Batuk, pilek, pusing',
        )
        self.form_data = {
            'name': 'Andre',
            'age': 30,
            'sex': 'Male',
            'job': 'Petani',
            'city': 'Depok',
            'province': 'Jawa Barat',
            'medical_history': 'Asma',
            'education': 'SMA',
            'symptoms': 'Batuk, pilek, pusing',
        }

    def test_form_GET(self):
        response = self.client.get(self.form_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form/form.html')

    def test_form_POST(self):
        response = self.client.post(self.form_url, self.form_data, follow=True)
        self.assertEqual(Patient.objects.last().name, 'Andre')

    def test_model_str(self):
        self.assertEqual(str(self.patient), 'Andre')

    def test_render_template_success(self):
        response = self.client.get(self.success_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form/success.html')
