from django.forms import ModelForm
from .models import Article, Image, Tags


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'author', 'text']

class TagsForm(ModelForm):
    class Meta:
        model = Tags
        fields = ['name', 'article']


class ImageForm(ModelForm):
    class Meta:
        model = Image
        fields = ['image']