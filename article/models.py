from django.db import models
import os

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    text = models.TextField()
    def __str__(self):
        return self.title

class Tags(models.Model):
    name = models.CharField(max_length=100)
    article = models.ForeignKey(
        Article, 
        on_delete=models.CASCADE, 
        related_name='tags'
    )
    def __str__(self):
        return self.name

class Image(models.Model):
    article = models.ForeignKey(
        Article, 
        on_delete=models.CASCADE, 
        related_name='image'
    )
    image = models.ImageField(default=None)